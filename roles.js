const roles = [
    { name: "Admin", administrationAccess: true, canEdit: true, canRead: true },
    { name: "Editor", administrationAccess: false, canEdit: true, canRead: true },
    { name: "Guest", administrationAccess: false, canEdit: false, canRead: true }
  ]

module.exports = roles;
