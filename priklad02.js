// Příklad 1
// napište funkci, která spojí uživatele s jeho právy

const users = require('./users.js')
const roles = require('./roles.js')

function joinUsersWithRights(arrayUsers, arrayRoles){
  let output = [];
  arrayUsers.forEach(element=>{
    arrayRoles.forEach(elem=>{
      if(element.role===elem.name){
        output.push({id: element.id, name: element.name, role: elem.name, administrationAccess: elem.administrationAccess, canEdit: elem.canEdit, canRead: elem.canRead});
      }
    })
  })
  return output;
}
console.log(joinUsersWithRights(users, roles));
  // Očekávaný výstup funkce (na pořadí položek uvnitř objektů nezáleží):
  /*[
    {
      id: 11,
      name: 'Pavel',
      role: 'Admin',
      administrationAccess: true,
      canEdit: true,
      canRead: true
    },
    {
      id: 22,
      name: 'Bára',
      role: 'Editor',
      administrationAccess: false,
      canEdit: true,
      canRead: true
    },
    {
      id: 33,
      name: 'Jana',
      role: 'Editor',
      administrationAccess: false,
      canEdit: true,
      canRead: true
    }
  ]*/
  // bonus: vytvořte si seznam administrátorů a seznam editorů a jako výsledek funkce vraťte opět seznam všech s jejich právy

  let admin = require('./administrators');
  let editor = require('./editors');
  let all =[
    ...admin,
    ...editor
  ]
  //console.log(all)

  console.log(joinUsersWithRights(all, roles))

