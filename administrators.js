const admin = [
    { id: 12, name: "Pavlina", role: "Admin"},
    { id: 23, name: "Barbara", role: "Admin"},
    { id: 34, name: "Jan", role: "Admin"}
]

module.exports = admin;