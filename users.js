const users = [
    { id: 11, name: "Pavel", role: "Admin" },
    { id: 22, name: "Bára", role: "Editor" },
    { id: 33, name: "Jana", role: "Editor" }
  ]

  module.exports = users;
